## [6.5.1] - 2019-08-07
### Fixed
- `initialBitrateListener` method wrongly using .bind()
- Modified `seekingListener` to make this non ambiguous when executing it unminified.
### Library
- Packaged with `lib 6.5.11`

## [6.5.0] - 2019-07-11
### Library
- Packaged with `lib 6.5.6`

## [6.4.0] - 2018-10-19
### Library
- Packaged with `lib 6.4.9`
