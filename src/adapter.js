var youbora = require('youboralib')
var manifest = require('../manifest.json')

youbora.adapters.Hasplayer = youbora.Adapter.extend({

  /** Override to return current plugin version */
  getVersion: function () {
    return manifest.version + '-' + manifest.name + '-' + manifest.tech
  },

  /** Override to return title as Unknown */
  getTitle: function () {
    return 'Unknown'
  },

  /** Override to return resource as Unknown */
  getResource: function () {
    return 'Unknown'
  },

  /** Override to return current playhead of the video */
  getPlayhead: function () {
    return this.player.getPosition()
  },

  /** Override to return dropped frames since start */
  getDroppedFrames: function () {
    return this.player.getVideoModel().getElement().webkitDroppedFrameCount
  },

  /** Override to return current playrate */
  getPlayrate: function () {
    return this.player.getVideoModel().getPlaybackRate()
  },

  /** Override to return video duration */
  getDuration: function () {
    return this.player.getDuration()
  },

  /** Override to return current bitrate */
  getBitrate: function () {
    return this.bitrate
  },

  /** Override to return rendition */
  getRendition: function () {
    if ((this.width && this.height) || this.bitrate) {
      return youbora.Util.buildRenditionString(this.width, this.height, this.bitrate)
    }
    return null
  },

  /** Override to return player version */
  getPlayerVersion: function () {
    return this.player.getVersion()
  },

  /** Override to return player's name */
  getPlayerName: function () {
    return 'Hasplayer'
  },

  registerListeners: function () {
    this.monitorPlayhead(true, false)
    this.references = {}
    this.references['play'] = this.playListener.bind(this)
    this.references['timeupdate'] = this.timeupdateListener.bind(this)
    this.references['pause'] = this.pauseListener.bind(this)
    this.references['playing'] = this.playingListener.bind(this)
    this.references['error'] = this.errorListener.bind(this)
    this.references['seeking'] = this.seekingListener.bind(this)
    this.references['seeked'] = this.seekedListener.bind(this)
    this.references['ended'] = this.endedListener.bind(this)
    this.references['play_bitrate'] = this.bitrateListener.bind(this)
    this.references['download_bitrate'] = this.initialBitrateListener.bind(this)
    this.references['manifestUrlUpdate'] = this.endedListener.bind(this)

    for (var key in this.references) {
      this.player.addEventListener(key, this.references[key])
    }
  },

  /** Unregister listeners to this.player. */
  unregisterListeners: function () {
    // Disable playhead monitoring
    if (this.monitor) this.monitor.stop()
    // unregister listeners
    if (this.player && this.references) {
      for (var key in this.references) {
        this.player.removeEventListener(key, this.references[key])
      }
      this.references = []
    }
  },

  /** Listener for 'play_bitrate' event. */
  bitrateListener: function (e) {
    if (e.detail.type === 'video') {
      this.bitrate = e.detail.bitrate
      this.width = e.detail.width
      this.height = e.detail.height
    }
  },

  /** Listener for 'download_bitrate' event. */
  initialBitrateListener: function (e) {
    if (!this.bitrate && !this.width && !this.height) {
      if (e.detail.type === 'video') {
        this.bitrate = e.detail.bitrate
        this.width = e.detail.width
        this.height = e.detail.height
      }
    }
  },

  /** Listener for 'play' event. */
  playListener: function (e) {
    if (!this.isBlocked) {
      this.fireStart()
    }
    this.isBlocked = false
  },

  /** Listener for 'timeupdate' event. */
  timeupdateListener: function (e) {
    if (this.getPlayhead() > 0.1) {
      this.fireJoin()
    }
  },

  /** Listener for 'pause' event. */
  pauseListener: function (e) {
    this.firePause()
  },

  /** Listener for 'playing' event. */
  playingListener: function (e) {
    this.fireResume()
    this.fireSeekEnd()
    this.fireBufferEnd()
    this.fireJoin()
  },

  /** Listener for 'error' event. */
  errorListener: function (e) {
    this.fireError(e.data.code, e.data.message)
    if (e.data.code === 'DOWNLOAD_ERR_CONTENT') {
      this.fireStop()
      if (!this.flags.isStarted) {
        this.blocked = true
      }
    }
  },

  /** Listener for 'seeking' event. */
  seekingListener: function (e) {
    if (this.getPlayhead() > 0.1) {
      this.fireSeekBegin()
    }
    /** Infinite seek player bug stopper */
    var now = new Date().getTime()
    if (this.seekList) {
      this.seekList.push(now)
    } else {
      this.seekList = [now]
    }
    if (this.seekList.length > 3) {
      this.seekList.slice(-4)
      if (this.seekList[this.seekList.length - 1] - this.seekList[this.seekList.length - 4] < 500) {
        this.fireStop()
      }
    }
  },

  /** Listener for 'seeked' event. */
  seekedListener: function (e) {
    this.fireSeekEnd()
  },

  /** Listener for 'ended' and 'manifestUrlUpdate' events. */
  endedListener: function (e) {
    this.fireStop()
    this.width = null
    this.height = null
    this.bitrate = null
  }
})

module.exports = youbora.adapters.Hasplayer
